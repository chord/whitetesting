﻿using System;
using System.Windows;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.MenuItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WindowStripControls;

namespace axis_open_files_hang_testing
{
	internal class Program
	{
		[STAThread]
		private static void Main(string[] args)
		{
			System.Windows.Forms.OpenFileDialog fFileOpenDialog = new System.Windows.Forms.OpenFileDialog();
			fFileOpenDialog.DefaultExt = ".raw";
			fFileOpenDialog.Filter = "Axion/NeuroRighter raw files (.raw)|*.raw";
			fFileOpenDialog.Multiselect = true;
			fFileOpenDialog.ShowDialog();
			string fFiles = string.Empty;
			foreach (string iPath in fFileOpenDialog.FileNames)
				fFiles += (fFiles.Length + iPath.Length) < 260 ? string.Format("\"{0}\"", iPath.Substring(0, iPath.LastIndexOf('.'))) : string.Empty;
			Console.WriteLine("Will open files: {0}", fFiles);

			Console.Write("PID of AxIS debug window: ");
			int fPID;
			int.TryParse(Console.ReadLine(), out fPID);
			//I really should make sure that fPID is valid here
			HangTesting(fPID, fFiles);
		}

		private static void HangTesting(int aPID, string aFiles)
		{
			Application fApp = Application.Attach(aPID);

			fApp.WaitWhileBusy();

			Window fWindow = fApp.GetWindow(SearchCriteria.ByText("Axion Integrated Studio (AxIS)"),
				TestStack.White.Factory.InitializeOption.WithCache);
			fWindow.WaitWhileBusy();

			fWindow.DisplayState = DisplayState.Restored;

			MenuBar menuBar = fWindow.MenuBar;
			Menu fOpenRecordingsMenuItem;
			var fStreamTree = fWindow.Get<Panel>(SearchCriteria.ByAutomationId("mwScrollViewer"));
			PopUpMenu fContextMenu;
			TextBox fInputBox;
			Window fFileDialog;
			Panel fOpenButton;

			int counter = 0;
			while (fWindow.IsCurrentlyActive)
			{
				fWindow.WaitWhileBusy();
				Point fStreamTreePoint = fStreamTree.Location;
				fStreamTreePoint.X += 1;
				fStreamTreePoint.Y += 1;
				fStreamTree.RightClickAt(fStreamTreePoint);
				fContextMenu = fWindow.Popup;
				fContextMenu.Items[0].Click();

				fWindow.WaitWhileBusy();

				fOpenRecordingsMenuItem = menuBar.MenuItem("File", "Open Recording(s)...");
				fOpenRecordingsMenuItem.SetValue(0);
				fOpenRecordingsMenuItem.Click();

				fWindow.WaitWhileBusy();

				fFileDialog = fWindow.ModalWindow("Open");
				fFileDialog.WaitWhileBusy();
				fInputBox = fFileDialog.Get<TextBox>(SearchCriteria.ByText("File name:"));
				fOpenButton = fFileDialog.Get<Panel>(SearchCriteria.ByAutomationId("1"));
				fInputBox.SetValue(aFiles);
				fOpenButton.Click();
				counter++;
				Console.WriteLine("Number of tests completed: {0}", counter);
			}
		}
	}
}
