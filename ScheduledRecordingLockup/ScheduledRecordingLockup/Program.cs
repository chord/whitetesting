﻿using System;
using System.IO.Ports;
using System.Threading;
using System.Windows;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.MenuItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WindowStripControls;

namespace ScheduledRecordingLockup
{
	class Program
	{
		
		private static void Main(string[] args)
		{
			int fPID, fDuration;
			Console.Write("PID of AxIS debug window: ");
			int.TryParse(Console.ReadLine(), out fPID);
			//I really should make sure that fPID is valid here
			HangTesting(fPID);
		}

		private static void HangTesting(int aPID)
		{
			Application fApp = Application.Attach(aPID);

			fApp.WaitWhileBusy();

			Window fWindow = fApp.GetWindow(SearchCriteria.ByText("Axion Integrated Studio (AxIS)"),
				TestStack.White.Factory.InitializeOption.WithCache);
			fWindow.WaitWhileBusy();

			fWindow.DisplayState = DisplayState.Restored;

			Label fStartButton = fWindow.Get<Label>(SearchCriteria.ByAutomationId("mwStartLabel"));
			TextBox fDurationBox = fWindow.Get<TextBox>(SearchCriteria.ByAutomationId("mwRecDurationTextBox"));
			ComboBox fDurationUnits = fWindow.Get<ComboBox>(SearchCriteria.ByAutomationId("mwRecDurationUnitsCB"));

			int fDurationSeconds;
			int.TryParse(fDurationBox.Text, out fDurationSeconds);
			switch (fDurationUnits.SelectedItemText)
			{
				case "Minutes":
					fDurationSeconds *= 60;
					break;
				case "Hours":
					fDurationSeconds *= 3600;
					break;
				case "Days":
					fDurationSeconds *= 86400;
					break;
				default:
					break;
			}

			fWindow.Click();

			int counter = 0;
			while (fWindow.IsCurrentlyActive)
			{
				try
				{
					fWindow.WaitWhileBusy();

					if (fStartButton.Enabled)
					{
						fStartButton.Click();
						Thread.Sleep((fDurationSeconds + 1) * 1000);
					}
					else
					{
						Thread.Sleep(10000);
						if (!fStartButton.Enabled)
						{
							string filename = string.Format("RunLog_{0:yyyy.MM.dd-HH.mm.ss}.txt", DateTime.Now), 
								logText = string.Format("Test ran {0} times.", counter);
							System.IO.File.WriteAllText(filename, logText);
							Locked();
						}
						return;
					}

					counter++;
					Console.WriteLine("Number of tests completed: {0}", counter);
				}
				catch (Exception e)
				{
					string filename = string.Format("RunLog_{0:yyyy.MM.dd-HH.mm.ss}.txt", DateTime.Now),
						logText = string.Format("Test ran {0} times.{1}{2}", counter, Environment.NewLine, e);
					System.IO.File.WriteAllText(filename, logText);
				}
			}
		}

		private static void Locked()
		{
			Console.WriteLine("AxIS locked up(maybe)!");
			Console.WriteLine("Press Enter to exit...");
			Console.ReadLine();
		}
	}
}
