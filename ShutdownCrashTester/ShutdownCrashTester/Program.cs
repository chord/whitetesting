﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestStack.White;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Custom;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.MenuItems;
using TestStack.White.UIItems.TreeItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WindowStripControls;

namespace ShutdownCrashTester
{
	class Program
	{
		private static void wait(int sec)
		{
			System.Threading.Thread.Sleep(sec * 1000);
		}

		static void Main(string[] args)
		{
			while (true)
			{
				Application fApp = 
					Application.Launch("C:/Program Files (x86)/Axion BioSystems, Inc/Axion Integrated Studio (AxIS)/Axion.AxIS.exe");
				//TODO make a check for file existance and ask for path if invalid
				fApp.WaitWhileBusy();
				Window fWindow = fApp.GetWindow(SearchCriteria.ByText("Axion Integrated Studio (AxIS)"),
					TestStack.White.Factory.InitializeOption.WithCache);
				fWindow.WaitWhileBusy();

				Image fPlay = fWindow.Get<Image>(SearchCriteria.ByAutomationId("mwPlayButton"));

				fPlay.Click();

				wait(10);

				fApp.Close();
			}

		}
	}
}
